# How to create new WP project in Digital Ocean Cloud using EasyDoFramework #

1. Fork this repository into your new [project name] repository
2. Clone the [project name] repository to your computer

3. Edit build.properties file
4. Set your project folder name in the line number 5; wp.url = http://46.101.163.174/projects/[project name]
5. Set your project database name in the line number 11; wp.database.name = [project name]
6. Set your WP instance title in the line number 25; wp.title = [title]

7. Create a new Jenkins Job from my-wp job
8. Change git repository to the [project name] repository
9. Change cp -r ${WORKSPACE}/. /var/www/html/projects/project1 to cp -r ${WORKSPACE}/. /var/www/html/projects/[project name]